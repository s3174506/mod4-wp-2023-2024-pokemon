package nl.utwente.mod4.pokemon.routes;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;
import nl.utwente.mod4.pokemon.dao.PokemonTypeDao;
import nl.utwente.mod4.pokemon.model.PokemonType;
import nl.utwente.mod4.pokemon.model.ResourceCollection;

@Path("/pokemon")
public class PokemonRoute {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceCollection<PokemonType> getPokemonTypes() {
        // initializes page size and page number for request
        int ps = Integer.MAX_VALUE;
        int pn = 1;
        // request the desired page and sorting to the Data Access Object
        PokemonType[] resources = PokemonTypeDao.INSTANCE.getPokemonTypes(ps, pn, "id").toArray(new PokemonType[0]);

        // updates the total and page size for the response
        int total = PokemonTypeDao.INSTANCE.getTotalPokemonTypes();
        ps = resources == null ? 0 : resources.length;

        // returns a collection object containing the requested resources
        return new ResourceCollection<>(resources, ps, pn, total);
    }
}