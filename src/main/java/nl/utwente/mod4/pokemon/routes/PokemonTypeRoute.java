package nl.utwente.mod4.pokemon.routes;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.util.List;
import nl.utwente.mod4.pokemon.dao.PokemonTypeDao;
import nl.utwente.mod4.pokemon.model.PokemonType;
import nl.utwente.mod4.pokemon.model.ResourceCollection;

@Path("/pokemonTypes")
public class PokemonTypeRoute {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceCollection<PokemonType> getPokemonTypes(
            @QueryParam("sortBy") String sortBy,
            @QueryParam("pageSize") int pageSize,
            @QueryParam("pageNumber") int pageNumber
    ) {
        // initializes page size and page number for request
        int ps = pageSize > 0 ? pageSize : Integer.MAX_VALUE;
        int pn = pageNumber > 0 ? pageNumber : 1;

        // request the desired page and sorting to the Data Access Object
        PokemonType[] resources = PokemonTypeDao.INSTANCE.getPokemonTypes(ps, pn, sortBy).toArray(new PokemonType[0]);

        // updates the total and page size for the response
        int total = PokemonTypeDao.INSTANCE.getTotalPokemonTypes();
        ps = resources == null ? 0 : resources.length;

        // returns a collection object containing the requested resources
        return new ResourceCollection<>(resources, ps, pn, total);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PokemonType createPokemonType(PokemonType pokemonType) {
        // creates a resource based on to the JSON object sent by the client
        return PokemonTypeDao.INSTANCE.create(pokemonType);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public PokemonType getPokemonType(@PathParam("id") String id) {
        // returns to the client the resource with the desired id
        return PokemonTypeDao.INSTANCE.getPokemonType(id);
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PokemonType updatePokemonType(@PathParam("id") String id, PokemonType toReplace) {
        // check if the desired id and the id of the resource to be replaced match
        if (id == null || !id.equals(toReplace.id))
            throw new BadRequestException("Id mismatch.");

        // return the object replaced
        return PokemonTypeDao.INSTANCE.replace(toReplace);
    }

    @DELETE
    @Path("/{id}")
    public void deletePokemonType(@PathParam("id") String id) {
        // delete the resource with the desired id
        PokemonTypeDao.INSTANCE.delete(id);
    }

}